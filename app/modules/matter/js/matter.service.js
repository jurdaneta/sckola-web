/**
 * Created by posma.marialejandra on 28/03/2017.
 */
(function(){
    'use strict';

    angular
        .module('sckola.matter')
        .factory('Matter', matter)
        .factory('SectionByCommunity',sectionByCommunity)
        .factory('ComunidadMaterias',comunidadMaterias)
        .factory('EstudiantesMateria',estudiantesMateria)
        .factory('AssociateMateriaSection',associateMateriaSection)
        .factory('MattersCommunity',mattersCommunity)
        .factory('AssociateMateriaCommunity',associateMateriaCommunity)
        .factory('CreateSection',createSection)
        .factory('evaluation', evaluation)
        .factory('EvaluationByMatterCommunitySection', evaluationByMatterCommunitySection)
        .factory('CreateQualificationByMatterSectionByEvaluation', createQualificationByMatterSectionByEvaluation)
        .factory('QualificationByStudentEdit', qualificationByStudentEdit)
        .factory('QualificationByStudent', qualificationByStudent)
        .factory('EvaluationCulminated', evaluationCulminated)
        .factory('ComunidadMateriasWithoutEvaluacionPlan', comunidadMateriasWithoutEvaluacionPlan)
        .factory('ComunidadMateriasByType', comunidadMateriasByType)
        .factory('scale', scale)
        .factory('clase', clase)
        .factory('GetAllClassFromTeacher', getAllClassFromTeacher)
        .factory('DesassociateMatterCommunitySection', desassociateMatterCommunitySection)
        .value('data', {});


    //OBTIENE TODAS LAS MATERIAS EXISTENTES
    matter.$inject = ['$resource','$rootScope'];
    function matter($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter');
        //return $resource($rootScope.domainUrl + '/app/static/data/matters.json');
    };

    //OBTIENE TODAS LAS SECCIONES DE UNA COMUNIDAD
    sectionByCommunity.$inject = ['$resource','$rootScope'];
    function sectionByCommunity($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/section/community/:communityId');
    };

    //PERMITE CREAR UNA NUEVA SECCION
    createSection.$inject = ['$resource','$rootScope'];
    function createSection($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/section/community/:communityId/userId/:userId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    //OBTIENE TODAS LAS MATERIAS EXISTENTES PARA UNA COMUNIDAD USUARIO ROL
    comunidadMaterias.$inject = ['$resource','$rootScope'];
    function comunidadMaterias($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter/community/:communityId/role/:roleId/user/:userId');
        //return $resource('static/data/communityMatters.json');
    };

    //OBTIENE TODAS LAS MATERIAS EXISTENTES PARA UNA COMUNIDAD USUARIO ROL POR TIPO
    comunidadMateriasByType.$inject = ['$resource','$rootScope'];
    function comunidadMateriasByType($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter/community/:communityId/role/:roleId/user/:userId/type/:type');
        //return $resource('static/data/communityMatters.json');
    };

    //OBTIENE TODAS LAS MATERIAS EXISTENTES PARA UNA COMUNIDAD
    mattersCommunity.$inject = ['$resource','$rootScope'];
    function mattersCommunity($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter/community/:communityId');
    };


    //PERMITE ASOCIAR MATERIA A COMUNIDAD
    associateMateriaCommunity.$inject = ['$resource','$rootScope'];
    function associateMateriaCommunity($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter/:matterId/community/:communityId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    //PERMITE ASOCIAR MATERIA A SECCION PROFESOR
    associateMateriaSection.$inject = ['$resource','$rootScope'];
    function associateMateriaSection($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter_community/:matterCommunityId/section/:sectionId/user/:userId',null,
        {
            save: {
                method: 'POST'
                /*headers: {
                 Authorization: 'Bearer '+ authentication.getToken()
                 }*/
            },
            get: {
                method: 'GET'
            } ,
            query: {
                method: 'GET',
                isArray:true
            }
        });
    };

    desassociateMatterCommunitySection.$inject = ['$resource','$rootScope'];
    function desassociateMatterCommunitySection($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter_community_section/:matterCommunitySectionId',null,
            {
                update: {
                    method: 'PUT'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    estudiantesMateria.$inject = ['$resource'];
    function estudiantesMateria($resource){
        return $resource('static/data/students.json');
    };

    evaluation.$inject = ['$resource'];
    function evaluation($resource){
        return $resource('static/data/plan_de_evaluacion.json');
    };

    scale.$inject = ['$resource'];
    function scale($resource){
        return $resource('static/data/scales_edit.json');
    };

    clase.$inject = ['$resource'];
    function clase($resource){
        return $resource('static/data/clases.json');
    };

    //Servicio que busca las evaluaciones de la clase
    evaluationByMatterCommunitySection.$inject = ['$resource','$rootScope'];
    function evaluationByMatterCommunitySection($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/matter_community_section/:matterId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que crea las calificaciones de todos los estudiantes de una materia comunity section a una evaluacion
    createQualificationByMatterSectionByEvaluation.$inject = ['$resource','$rootScope'];
    function createQualificationByMatterSectionByEvaluation($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/qualification/matter_community_section/:matterId/evaluation/:evaluationId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que edit una calificacion de un estudiante de una evaluacion
    qualificationByStudentEdit.$inject = ['$resource','$rootScope'];
    function qualificationByStudentEdit($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/qualification/:qualificationId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que Buscar una calificacion de un estudiante de una evaluacion
    qualificationByStudent.$inject = ['$resource','$rootScope'];
    function qualificationByStudent($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/qualification/matter_community_section/:matterId/user/:userId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que cambia el estatus de la evaluacion a culminada
    evaluationCulminated.$inject = ['$resource','$rootScope'];
    function evaluationCulminated($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/evaluation/:evaluationId/culminated',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //OBTIENE TODAS LAS MATERIAS EXISTENTES PARA UNA COMUNIDAD USUARIO ROL Que no tengan Plan de evaluacion
    comunidadMateriasWithoutEvaluacionPlan.$inject = ['$resource','$rootScope'];
    function comunidadMateriasWithoutEvaluacionPlan($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter/community/:communityId/role/:roleId/user/:userId/without_evaluation_plan');
        //return $resource('static/data/communityMatters.json');
    };

    /**
     * Get all classes that a teacher
     * @type {string[]}
     */
    getAllClassFromTeacher.$inject = ['$resource', '$rootScope'];
    function getAllClassFromTeacher($resource, $rootScope){
        return $resource($rootScope.domainUrl+'/skola/matter/user/:userId');
    };

})();