(function(){
    'use strict';

    angular
        .module("sckola.uploadNotes", ['ui.router','ui.bootstrap'])
        .run(addStateToScope)
        .config(getRoutes);


    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('uploadNotes', {
                parent: 'index',
                url: '/uploadNotes',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'content@': {
                        templateUrl: "modules/uploadNotes/partials/uploadNotes.html",
                        controller: "UploadNotesCTRL",
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }

            })

    };
})();