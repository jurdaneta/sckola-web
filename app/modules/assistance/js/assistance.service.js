(function(){

    'use strict';

    angular.module('sckola.assistance')
        .factory('AssistanceStudentsEdit', assistanceStudentsEdit)
        .factory('AssistanceStudents', assistanceStudents);

    //Servicio que crea las asistencias de una materia y busca las assistencia por fecha de los estudiantes de la materia
    assistanceStudents.$inject = ['$resource','$rootScope'];
    function assistanceStudents($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/assistance/matter_community_section/:matterCommunityId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //Servicio que Edita la assitencia de un estudiante
    assistanceStudentsEdit.$inject = ['$resource','$rootScope'];
    function assistanceStudentsEdit($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/skola/assistance',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

})();
