(function(){
    'use strict';

    angular
        .module('sckola.assistance', ['ui.router','ui.bootstrap'])
        .run(addStateToScope)
        .config(getRoutes);

    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('assistances', {
                parent: 'index',
                url: '/assitances',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'content@':{
                        templateUrl: 'modules/assistance/partials/assistances.html',
                        controller: 'AssistanceCTRL',
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
    };
})();
