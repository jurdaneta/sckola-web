(function(){
  'use strict';

  angular
  .module('sckola.evaluationPlan')
  .controller('EvaluationPlanCtrl', EvaluationPlanCtrl);

  EvaluationPlanCtrl.$inject = ['authentication','$rootScope','$scope','$modal','$state','data','Scales','EvaluationTools','ComunidadMateriasWithoutEvaluacionPlan', 'EvaluationPlanFromTeacher','EvaluationPlanDetail', 'CreateEvaluationPlan', 'CreateEvaluation', 'AssociatePlanEvaluationClass', 'UpdateEvaluation', 'EvaluationPlanTemplate', '$timeout', 'ComunidadMaterias'];
  function EvaluationPlanCtrl(authentication,$rootScope,$scope,$modal,$state,data,Scales,EvaluationTools,ComunidadMateriasWithoutEvaluacionPlan, EvaluationPlanFromTeacher, EvaluationPlanDetail, CreateEvaluationPlan, CreateEvaluation, AssociatePlanEvaluationClass, UpdateEvaluation, EvaluationPlanTemplate, $timeout, ComunidadMaterias){
    var vm = this;
    vm.messageInitial = "Por favor seleccione una comunidad";
    vm.messageSaveEvaluationPlan = "¡Felicitaciones! Has creado un nuevo plan de evaluación";
    vm.messageAsociate = "¡Felicitaciones! has asociado correctamente el plan de evaluación a la clase";
    vm.showMessageInitial = true;
    vm.disabledSeccionNew = false;
    vm.showError = false;
    vm.errorMessage = "¡Oops! Algo ha salido mal";
    vm.messageHelp = "";
    vm.showHelp = false;
    vm.plantillaPreexistente = null;
    vm.successMessage = "";
    vm.showSuccessMessage = false;
    vm.showMessage = false;
    vm.messageEdit = false;
    vm.user = authentication.getUser();
    vm.community = authentication.getCommunity();
    var permission = authentication.currentUser();
    $rootScope.tokenLoging = authentication.isLoggedIn();

    if(vm.community==null){
      vm.showHelp = true;
      vm.helpMessage = "Debes seleccionar una comunidad. ¡Para seleccionar una comunidad haga click aqui!";
    }

    /**
    * @name showErrorMessage
    * @desc Shows error message to the user
    * @memberOf Controllers.EvaluationPlanController
    */
    vm.showErrorMessage = function(){
      vm.showError = true;
      $timeout(function(){
        vm.showError = false;
      },2500);
    };


    /**
    * @name helpUser
    * @desc Depending of the user state, if it has a community or not, a mmater or not,
    *       the state of the view is changed to the requirement.
    * @memberOf Controllers.EvaluationController
    */
    vm.helpUser = function(){
      if(vm.community == null){
        $state.go("communities");
      }
      else if(vm.communityMatters<1){
        $state.go("matterHome");
      }
    };

    vm.loadInitial = function(){

      vm.showMessageInitial = false;
      if (vm.community !== undefined && vm.community.id !== undefined) {
        EvaluationPlanFromTeacher.get({userId: vm.user.id, communityId: vm.community.communityOrigin.id },
          function(success){
            vm.evaluationPlan = success.response;
            vm.showMessageInitial = false;
            $timeout(function(){
              vm.terminate = true;
            }, 1000);
          },
          function(error){
            vm.showErrorMessage();
          });

          Scales.get({},
            function(success){
              vm.scales = success.response;
            }, function(error){
              vm.message = "Error cargado data de scales"
            }
        );
        EvaluationTools.get({},
            function(success){
              vm.evaluationTools = success.response;
            }, function(error){
          vm.message = "Error cargado data de evalucion tools"
        });

        EvaluationPlanTemplate.get({},
            function(success){
              vm.evaluationTemplate = success.response;
            },
            function(error){
              vm.message = "Error en la carga de materias"
            }
        );

        ComunidadMaterias.get({communityId: vm.community.communityOrigin.id, roleId: 1, userId: vm.user.id},
            function (success) {
              vm.communityMattersAll = success.response;

              ComunidadMateriasWithoutEvaluacionPlan.get({communityId:vm.community.communityOrigin.id,roleId:1,userId:vm.user.id},
                  function(success){
                    vm.communityMatters = success.response;
                    if(vm.communityMatters.length == 0 && vm.communityMattersAll.length == 0){
                      vm.showHelp = true;
                      vm.helpMessage = "No posees clases asociadas en esta comunidad. ¡Para asociar una clase haga click aquí!";
                    }
                    else{
                      vm.matterExist = true;
                      vm.messageMatterLoad="No existen clases sin un plan de evaluaci\u00F3n";}
                  },
                  function(error){
                    vm.message = "Error cargado data de comunidad"
                  }
              );
            },
            function(error){
              vm.message = "Error cargado data de evalucion tools";
              vm.showErrorMessage();
            });

            EvaluationPlanTemplate.get({},
              function(success){
                vm.evaluationTemplate = success.response;
              },
              function(error){
                vm.message = "Error en la carga de materias";
                vm.showErrorMessage();
              }
            );
          }else{
            vm.terminate = true;
            vm.showMessageInitial = true;
          }


        };

        //Valida si el usuario esa autenticado con los permisos para la vista
        if (permission === null || authentication.getUser() === null) {
          $state.go('root');
          console.log("no esta autenticado:" + permission + " JSON:" + permission!==undefined?JSON.stringify(permission):"null");
        }else{
          // carga inicial por defecto
          vm.loadInitial();
        }

    //escucha cuando exista cambio en la comunidad seleccionada
    $scope.$on('community', function (evt, community) {
      vm.community = community;
      vm.loadInitial();
    });

    vm.cancel = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.go = function(){
      $state.go('teacherHome');
    };

    vm.evaluationPlanNew = function (){
      vm.planEvaluacion =[];
      vm.evaluationName=null;
      vm.cuentaValor = 0;
      vm.plantillaPreexistente = null;
      $scope.modalInstance = $modal.open({
        templateUrl: 'modules/evaluation/partials/modal/newEvaluationPlan_modal.html',
        scope: $scope,
        size: 'md',
        keyboard  : false
      });
    };

    vm.saveEvaluationPlan = function () {
      vm.evaluationPlanNuevo={"name":vm.evaluationName,"userId":vm.user.id};
      CreateEvaluationPlan.save({},vm.evaluationPlanNuevo,
          function(success){
            for(var i=0;i<vm.planEvaluacion.length;i++){
              vm.planEvaluacion[i].date = vm.planEvaluacion[i].dateNew.getDate()+"-"+(vm.planEvaluacion[i].dateNew.getMonth()+1)+"-"+vm.planEvaluacion[i].dateNew.getFullYear();
              vm.createEvaluation(vm.planEvaluacion[i], success.response.id);
            }
            vm.evaluationPlan.push(success.response);
            $scope.modalInstance.dismiss('cancel');
            vm.showNotification(vm.messageSaveEvaluationPlan);
          },function(error){
          });
    };

    vm.showNotification = function(message){
      vm.successMessage = message;
      vm.showSuccessMessage = true;
      $timeout(function(){
        vm.showSuccessMessage = false
      },2500);
    };

    vm.createEvaluation = function(evaluation, evaluationPlanId){
      CreateEvaluation.save({planId:evaluationPlanId},evaluation,
          function(success){
          },function(error){
          });
    };

    vm.editMatter = function (){
      $scope.modalInstance = $modal.open({
        templateUrl: 'modules/evaluation/partials/modal/newEvaluationPlan_modal.html',
        scope: $scope,
        size: 'md',
        keyboard  : false
      });
    };

    vm.detailPlan = function(item){
      vm.namePlan = item.name;
      vm.evaluationPlanDetail = item;
      vm.cuentaValor = 0;
      EvaluationPlanDetail.get({planId: item.id},
          function(success){
            vm.evaluations = success.response.evaluations;
            for(var b=0;b < vm.evaluations.length;b++){
              var formato = vm.evaluations[b].date.split("-");
              vm.evaluations[b].dateNew = new Date(formato[2],formato[1]-1,formato[0]);
              vm.cuentaValor = parseInt(vm.cuentaValor)+ parseInt(vm.evaluations[b].weight);
            }
            $scope.modalInstance = $modal.open({
              templateUrl: 'modules/evaluation/partials/modal/detailEvaluationPlan_modal.html',
              scope: $scope,
              size: 'lg',
              keyboard  : false
            });
          },
          function(error){
          });
    };

    vm.associatePlanEvaluationClase = function(clase) {
      if (clase.id != null){
        vm.associate = {"associate": true};
        AssociatePlanEvaluationClass.save({
              planId: vm.evaluationPlanAssociate.id,
              matterCommunitySectionId: clase.id
            }, vm.associate,
            function (success) {
              vm.evaluationPlanAssociate.matterCommunitySection = vm.class;
              $scope.modalInstance.dismiss('cancel');
              vm.showNotification(vm.messageSaveEvaluationPlan);
            },function(error){
              vm.showErrorMessage();
            });
      }
    };

    vm.associatePlanClase = function(item){
      vm.evaluationPlanAssociate=item;
      vm.matterExist = false;
      ComunidadMateriasWithoutEvaluacionPlan.get({communityId:vm.community.communityOrigin.id,roleId:1,userId:vm.user.id},
          function(success){
            vm.communityMatters = success.response;
            if(vm.communityMatters.length<1){
              vm.showHelp = true;
              vm.helpMessage = "No posees clases asociadas en esta comunidad. ¡Para asociar una clase haga click aquí!";
            }
            else{
              vm.matterExist = true;
              vm.messageMatterLoad="No existen clases sin un plan de evaluaci\u00F3n";
            }
          },
          function(error){
            vm.message = "Error cargado data de comunidad"
          }
      );
      EvaluationPlanDetail.get({planId: item.id},
          function(success){
            vm.cuentaValor = 0;
            vm.evaluations = success.response.evaluations;
            for(var b=0;b < vm.evaluations.length;b++){
              vm.cuentaValor = parseInt(vm.cuentaValor)+ parseInt(vm.evaluations[b].weight);
            }
            if(vm.cuentaValor != 100){
              vm.matterExist = false;
              vm.messageMatterLoad = "El plan de evaluaci\u00F3n no esta completo";
            }
            $scope.modalInstance = $modal.open({
              templateUrl: 'modules/evaluation/partials/modal/associatePlanToClass_modal.html',
              scope: $scope,
              size: 'lg',
              keyboard  : false
            });
          },
          function(error){
          });
    };

    vm.addEvaluation = function()
    {
      vm.evaluation={"objective":null,"evaluationTool":null,"evaluationScale":null,"date":null,"weight":null};
      vm.planEvaluacion.push(vm.evaluation);
    };

    vm.addEvaluationInEdit = function()
    {
      vm.evaluation={"objective":null,"evaluationTool":null,"evaluationScale":null,"date":null,"weight":null,"editar":true,'dateNew':null};
      vm.evaluations.push(vm.evaluation);
    };

    vm.quitarEvaluation = function(evaluacion){
      var indexOfEvaluation = vm.planEvaluacion.indexOf(evaluacion);
      vm.planEvaluacion.splice(indexOfEvaluation, 1);
      vm.completeValor();
    };

    vm.eliminarEvaluation = function(evaluacion){
      vm.evaluation = evaluacion;
      if(vm.evaluation.id != null){
      UpdateEvaluation.delete({evaluationId: evaluacion.id},null,
          function(success){
            var indexOfEvaluation = vm.evaluations.indexOf(vm.evaluation);
            vm.evaluations.splice(indexOfEvaluation, 1);
            vm.completeValorDetail();
          },
          function(error){
          });
      }else{
        var indexOfEvaluation = vm.evaluations.indexOf(vm.evaluation);
        vm.evaluations.splice(indexOfEvaluation, 1);
      }
    };

    vm.completeValorDetail = function(){
      vm.cuentaValor = 0;
      for(var i=0; i<vm.evaluations.length;i++){
        vm.cuentaValor = parseInt(vm.cuentaValor)+ parseInt(vm.evaluations[i].weight);
      }
    };

    vm.editEvaluation = function(evaluation){
      if(evaluation.objective != undefined && evaluation.objective != null
          && evaluation.evaluationTool != undefined && evaluation.evaluationTool != null
          && evaluation.evaluationScale != undefined && evaluation.evaluationScale != null
          && evaluation.dateNew != undefined && evaluation.dateNew != null
          && evaluation.weight != undefined && evaluation.weight != null){
        if(evaluation.id != undefined || evaluation.id != null){
          vm.evaluacion = evaluation;
          vm.evaluacion.date = vm.evaluacion.dateNew.getDate()+"-"+(vm.evaluacion.dateNew.getMonth()+1)+"-"+vm.evaluacion.dateNew.getFullYear();
          UpdateEvaluation.update({evaluationId:evaluation.id},vm.evaluacion,
              function(success){
                vm.evaluacion.editar = false;
                vm.completeValorDetail();
                vm.showMessage = true;
                vm.messageEditError = "¡Felicidades! se edito satisfactoriamente";
                vm.showmessageEdit = true;
                $timeout(function(){
                  vm.showmessageEdit = false;
                },2500);
              },function(error){
              });
        }else{
          vm.evaluacionNew = evaluation;
          vm.evaluacionNew.date = vm.evaluacionNew.dateNew.getDate()+"-"+(vm.evaluacionNew.dateNew.getMonth()+1)+"-"+vm.evaluacionNew.dateNew.getFullYear();
          CreateEvaluation.save({planId:vm.evaluationPlanDetail.id},vm.evaluacionNew,
              function(success){
                vm.evaluacionNew.id = success.response.id;
                vm.evaluacionNew.status = success.response.status;
                vm.evaluacionNew.editar = false;
                vm.evaluacionNew.evaluationPlanId = vm.evaluationPlanDetail.id;
                vm.completeValorDetail();
                vm.showMessage = true;
                vm.messageEditSuccess = "¡Felicidades! se guardo la evaluación";
                vm.showmessageEdit = true;
                $timeout(function(){
                  vm.showmessageEdit = false;
                },2500);
              },function(error){
              });
        }
      }else{
        vm.showMessage = true;
        vm.messageEditError = "Falta completar los campos de la evaluación";
        vm.messageEdit = true;
        $timeout(function(){
          vm.messageEdit = false;
        },2500);
      }
    };

    vm.completeValor = function(){
      vm.cuentaValor = 0;
      for(var i=0; i<vm.planEvaluacion.length;i++){
        vm.cuentaValor = parseInt(vm.cuentaValor)+ parseInt(vm.planEvaluacion[i].weight);
      }
    };

    vm.viewTemplate = function(){
      vm.planEvaluacion = [];
      if (vm.plantillaPreexistente!=undefined && vm.plantillaPreexistente.evaluations)
      {   var i=0;
        vm.cuentaValor = 0;
        vm.evaluationName = vm.plantillaPreexistente.name;
        while (i<vm.plantillaPreexistente.evaluations.length)
        {
          if (vm.plantillaPreexistente.evaluations[i].date != undefined)
          {
            var parts =vm.plantillaPreexistente.evaluations[i].date.split('-');
            var myDate = new Date(parts[2],parts[0]-1,parts[1]);
            vm.fechaDate = new Date(myDate);
          }
          vm.plantillaPreexistente.evaluations[i].dateNew = vm.fechaDate;
          vm.cuentaValor = parseInt(vm.cuentaValor)+ parseInt(vm.plantillaPreexistente.evaluations[i].weight);
          vm.planEvaluacion.push(vm.plantillaPreexistente.evaluations[i]);
          i++;
        }
      }
    };

    vm.validarFecha = function(evaluacion){
      var date = new Date();
      if(evaluacion.dateNew < date){
        evaluacion.dateNew = null;
      }
    }
  }
})();
