/**
 * Evaluation Plan Module
 * @namespace Modules
 */
(function(){
    'use strict';

    angular
        .module('sckola.evaluationPlan', ['ui.router','ui.bootstrap'])
        .run(addStateToScope)
        .config(getRoutes);

    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('evaluationPlan', {
                parent: 'index',
                url: '/evaluationsPlan',
                views:{
                    'navbar@': {
                        templateUrl: 'modules/navbar/partials/navbar.html',
                        controller: 'NavBarCtrl',
                        controllerAs: 'vm'
                    },
                    'sidemenu@':{
                        templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                        controller: 'SideMenuCtrl',
                        controllerAs: 'vm'
                    },
                    'content@':{
                        templateUrl: 'modules/evaluation/partials/evaluationsPlan.html',
                        controller: 'EvaluationPlanCtrl',
                        controllerAs: 'vm'
                    },
                    'footer@': {
                        templateUrl: '',
                        controller: '',
                        controllerAs: ''
                    },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
    };
})();
