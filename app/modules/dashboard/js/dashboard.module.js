
/**
* Modules
* @namespace Modules
*/
(function(){
  'use strict';

  angular
  .module("sckola.dashboard",['ui.router'])
  .run(addStateToScope)
  .config(getRoutes)

  addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
  /**
  *
  * @name addStateToScope
  * @desc Adds State to Scope.
  * @param {object} $rootScope - RootScope.
  * @param {object} $state - State of the module.
  * @param {object} $stateParams - Params of the state.
  * @memberOf Module.DashboardModule
  */
  function addStateToScope($rootScope, $state, $stateParams){
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  };

  getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
  /**
  *
  * @name getRoutes
  * @desc Gets all the routes of the module
  * @param {object} $rootScope - RootScope.
  * @param {object} $state - State of the module.
  * @param {object} $stateParams - Params of the state.
  * @memberOf Module.DashboardModule
  */
  function getRoutes($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('dashboard', {
      parent: 'index',
      url: '/dashboard',
      views:{
        'navbar@': {
          templateUrl: 'modules/navbar/partials/navbar.html',
          controller: 'NavBarCtrl',
          controllerAs: 'vm'
        },
        'sidemenu@':{
          templateUrl: 'modules/sidemenu/partials/sidemenu.html',
          controller: 'SideMenuCtrl',
          controllerAs: 'vm'
        },
        'content@':{
          templateUrl: 'modules/feed/partials/feed.html',
          controller: 'FeedCtrl',
          controllerAs: 'vm'
        },
        'footer@': {
          templateUrl: '',
          controller: '',
          controllerAs: ''
        },
        'news@': {
          templateUrl: 'modules/news/partials/news.html',
          controller: 'NewsCtrl',
          controllerAs: 'vm'
        }
      }
    })
  };
})();
