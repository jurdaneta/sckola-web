/**
 * Community Module
 * @namespace Modules
 */
(function(){
  'use strict';

  angular
    .module('sckola.community', ['ui.router','ui.bootstrap'])
    .run(addStateToScope)
    .config(getRoutes);

    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
      function addStateToScope($rootScope, $state, $stateParams){
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
      };

      getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
        function getRoutes($stateProvider, $urlRouterProvider){

          $urlRouterProvider.otherwise('/');
          $stateProvider
            .state('communities', {
                parent: 'index',
                url: '/communities',
                views:{
                  'navbar@': {
                            templateUrl: 'modules/navbar/partials/navbar.html',
                            controller: 'NavBarCtrl',
                            controllerAs: 'vm'
                          },
                  'sidemenu@':{
                            templateUrl: 'modules/sidemenu/partials/sidemenu.html',
                            controller: 'SideMenuCtrl',
                            controllerAs: 'vm'
                          },
                  'content@':{
                            templateUrl: 'modules/community/partials/communities.html',
                            controller: 'CommunityCtrl',
                            controllerAs: 'vm'
                          },
                'footer@': {
                              templateUrl: '',
                              controller: '',
                              controllerAs: ''
                            },
                    'news@': {
                        templateUrl: 'modules/news/partials/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm'
                            }
                        }
            })
      };
})();
