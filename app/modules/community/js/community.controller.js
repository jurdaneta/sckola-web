/**
* Controllers
* @namespace Controllers
*/
(function(){

  'use strict';

  angular.module('sckola.community')
  .controller('CommunityCtrl',CommunityCtrl);

  CommunityCtrl.$inject = ['$scope','Communities','ComunnityAssociate','authentication','$rootScope', '$timeout'];
  /**
  * @namespace CommunityController
  * @desc Controller for community
  * @memberOf Controllers
  */
  function CommunityCtrl($scope, Communities, CommunityAssociate, authentication,$rootScope, $timeout){

    var vm = this;
    vm.user = authentication.getUser();
    vm.communities = [];
    vm.successMessage = "Success!!";
    vm.showSuccessMessage = false;
    vm.messageHelp = "";
    vm.showHelp = false;
    vm.errorMessage = "¡Oops! Algo ha salido mal";
    vm.showError = false;
    var permission = authentication.currentUser();
    vm.community = authentication.getCommunity();
    $rootScope.tokenLoging = authentication.isLoggedIn();

    Communities.get({},
      function(success){
        vm.communities = success.response;
      },function(error){
        vm.errorMessage = "Ha ocurrido un error cargando las comunidades";
      });

      if(vm.community==null){
        vm.showHelp = true;
        vm.helpMessage = "Parece que no posees una comunidad. ¡Selecciona una comunidad de la lista desplegable!";
      }

      /**
      * @name showErrorMessage
      * @desc Shows error message to the user
      * @memberOf Controllers.CommunityController
      */
      vm.showErrorMessage = function(){
        vm.showError = true;
        $timeout(function(){
          vm.showError = false;
        },2500);
      };

      /**
      * @name helpUser
      * @desc Depending of the user state, if it has a community or not, a mmater or not,
      *       the state of the view is changed to the requirement.
      * @memberOf Controllers.CommunityController
      */
      vm.helpUser = function(){

      };

      /**
      * @name associateCommunity
      * @desc associates a community to the teacher.
      * @memberOf Controllers.CommunityController
      */
      vm.associateCommunity = function(){
        vm.associate = {associate:true};
        CommunityAssociate.save({communityId:vm.community.id,userId:vm.user.id,roleId:1},vm.associate,function(success){
          vm.successMessage = "¡Felicidades! Has asociado una comunidad exitosamente";
          vm.showSuccessMessage = true;
          if(vm.showHelp){
            vm.showHelp = false;
          }
          $scope.$parent.$broadcast('newCommunity', success.response);
          console.log("Succesfully associated");
        },function(error){
          vm.showErrorMessage();
        })
      }

    }
  })();
