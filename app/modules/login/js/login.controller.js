(function(){
    'use strict';

    angular
        .module('sckola.login')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$location','$scope', '$modal', '$state', '$rootScope', 'authentication', 'Login', 'UserRegistration', 'ValidateUserRegistration', 'RecoverPassword'];
    function LoginCtrl ($location, $scope, $modal, $state, $rootScope, authentication, Login, UserRegistration, ValidateUserRegistration, RecoverPassword){
        var vm = this;
        vm.credentials = {};
        vm.icono = "!";
        vm.newUser = {
                    "mail":"",
                    "password":""
                };

        vm.login = {
            "username":"",
            "password":""
        };

        //$rootScope.tokenLoging = undefined;

        authentication.logout();

        vm.acceptButton = false;
        vm.cancelButton = false;
        vm.okButton = false;
        vm.okGo = false;

        vm.openModalMessage = function(){
            $scope.modalInstance = $modal.open({
                templateUrl: 'template/modal/modal_message.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                size: 'sm',
                keyboard  : false
            });
        };

        // consulta si esta realizando una activacion de cuenta
        vm.code = $location.absUrl().split('=')[1];
        if(vm.code != undefined){
            vm.title = "Registro";
            vm.message = "Estamos activando su cuenta ...";

            vm.openModalMessage();

            ValidateUserRegistration.get({code: vm.code},
                function(success){
                    vm.message = "¡Felicitaciones!";
                    vm.message2 = "Ha completado su registro, su cuenta esta activa";
                    vm.okGo = true;
                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                },
                function(error){
                    var message = error.data.message;
                    vm.message = "Hubo problemas al activar su cuenta";

                    //The code is not valid, this may be that the code has already been used or does not exist
                    if(message != undefined && message.indexOf("The code is not valid") !== -1) {
                        vm.message = "C\u00F3digo de validaci\u00F3n incorrecto";
                        vm.message = "El c\u00F3digo pudo haber sido utilizado anteriormente y ya no existen en el sistema";
                    }

                    if(message != undefined && message.indexOf("The code is no longer valid") !== -1) {
                        vm.message = "C\u00F3digo de activaci\u00F3n ha expirado";
                        vm.message2 = "Ha esperado mucho tiempo para activar su cuenta. Debes volver a crear la cuenta en el sistema";
                    }
                    vm.okGo = true;
                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                });

        }

        vm.submit = function() {
                var login = {
                    "username": vm.login.username,
                    "password": vm.login.password
                };
                Login.save(vm.login,
                    function(success){
                        //Guarda response de login en el cliente
                        authentication.saveToken(success.response.token);
                        authentication.saveUser(success.response.userDetail);
                        $rootScope.tokenLoging = authentication.isLoggedIn();

                        if(success.response.userDetail.status === "WIZARD")
                            $state.go('dashboard');

                        if(success.response.userDetail.status === "ACTIVE")
                            $state.go('dashboard');
                    },
                    function(error){
                        if (error.status == 412)
                            vm.message = "Disculpe su usuario '"+vm.login.username + "' se encuentra INACTIVO.";
                        else if (error.status == 401)
                            vm.message = 'Usuario/Clave incorrecto. Por favor intente de nuevo.';
                        else
                            vm.message = 'Disculpe, en éstos momentos no puede acceder al sistema. Por favor, revise su usuario e intente de nuevo.';

                        vm.title = "Login";
                        vm.message2 = "";
                        vm.acceptButton = false;
                        vm.cancelButton = false;
                        vm.okButton = true;
                        vm.okGo = false;
                        vm.openModalMessage();
                    });


        };


        
        vm.openRegisterUser = function(){
            $scope.modalInstanceRegistro = $modal.open({
                templateUrl:
                    'modules/login/partials/modal/userReg.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                keyboard: false
            });
        };

        vm.registerUser = function(){
            vm.title = "Registro de Usuario";
            vm.message = 'Registro de usuario en progreso...';
            vm.message2 = "";
            vm.acceptButton = false;
            vm.cancelButton = false;
            vm.okButton = false;
            vm.openModalMessage();

            UserRegistration.save({},vm.newUser,
                function(success){
                    //console.log("respuesta registro:"+JSON.stringify(success))
                    vm.message = "Su cuenta ha sido creada";
                    vm.message2 = "La activaci\u00F3n de su cuenta fue enviada al correo electr\u00F3nico";
                    vm.okButton = true;

                    $scope.modalInstanceRegistro.dismiss('cancel');
                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                },
                function(error){
                    //console.log(error);
                    if(error.data.message.indexOf("Already exists user")!=-1){
                        vm.message = "El usuario ya existe";
                    }else{
                        vm.message = "Error de Registro de Usuario";
                    }
                    vm.message2 = "";
                    vm.cancelButton = false;

                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                });
        };

        vm.openRecoverPass = function(){
            $scope.modalInstanceRecover = $modal.open({
                templateUrl:
                    'modules/login/partials/modal/recoverPassword.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                keyboard: false
            });
        };

        vm.recoverPassword = function(){
            // correo a recuperar contraseña:
            //vm.newUser.mail

            vm.title = "Recuperar Contraseña";
            vm.message = 'Recuperación de contraseña en progreso...';
            vm.message2 = "";
            vm.acceptButton = false;
            vm.cancelButton = false;
            vm.okButton = false;
            vm.openModalMessage();

            if(vm.newUser.mail !== undefined){
                RecoverPassword.get({mail:vm.newUser.mail},vm.newUser,
                    function(success){
                        //console.log("respuesta registro:"+JSON.stringify(success))
                        vm.message = "Su contraseña fue enviada a su correo electrónico";
                        vm.okButton = true;

                        $scope.modalInstanceRecover.dismiss('cancel');
                        if($scope.modalInstance.result.$$state.status === 2){
                            vm.openModalMessage();
                        }
                    },
                    function(error){
                        if(error.data != undefined && error.data.message != undefined && error.data.message.indexOf("Parameter invalid")!=-1){
                            vm.message = "El correo no es valido";
                        }else{
                            vm.message = "Error de Registro de Usuario";
                        }
                        vm.message2 = "";
                        vm.cancelButton = false;

                        if($scope.modalInstance.result.$$state.status === 2){
                            vm.openModalMessage();
                        }
                    });

            }else{
                alert("El correo no puede estar vacio");
            }


        };

        vm.ok = function () {
            $state.reload();
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRegistro !== undefined)
                $scope.modalInstanceRegistro.close('close')
        };

        vm.go = function () {
            $state.go('root');
        };

        vm.cancel = function () {
            $scope.modalInstance.close('close');
        };

        vm.close = function () {
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRegistro !== undefined)
                $scope.modalInstanceRegistro.close('close')
        };

        vm.closeRecover = function () {
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRecover !== undefined)
                $scope.modalInstanceRecover.close('close')
        };

        vm.emailValidation = function(email){
            vm.email = email.$viewValue;
            vm.expresion = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!vm.expresion.test(vm.email)){
                vm.error = true;
            }else{
                vm.error = false;
            }
            if(vm.email.length < 6){
                vm.warning = true;
            }else{
                vm.warning = false;
            }
        };

        vm.passwordValidation = function(password){
            vm.password = password.$viewValue;
            if(vm.password.length < 6){
                vm.errorPassword = true;
            }else{
                vm.errorPassword = false;
                if(vm.password.length > 12){
                    vm.warningPassword = true;
                }else{
                    vm.warningPassword = false;
                }
            }
        };

    }

})();
