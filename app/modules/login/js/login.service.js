(function(){
    'use strict';

    angular
        .module('sckola.login')
        .factory('Login', Login)
        .factory('RecoverPassword', recoverPassword);

    Login.$inject = ['$resource','$rootScope'];
    function Login($resource, $rootScope){
        return $resource($rootScope.domainUrl+'/skola/login');
    };

    recoverPassword.$inject = ['$resource','$rootScope'];
    function recoverPassword($resource,$rootScope){
        return $resource($rootScope.domainUrl + '/skola/user/recover',{ mail: '@mail' });
    };



})();
